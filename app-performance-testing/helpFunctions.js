var jwtDecode = require('jwt-decode');

module.exports = {
  extractProfileId: extractProfileId,
  extractPurchaseIdOrUrl: extractPurchaseIdOrUrl
}

function extractProfileId(requestParams, response, context, ee, next) {
  var jwtToken = context.vars.jwtToken;
  var jwtData = jwtDecode(jwtToken);
  context.vars.profileId = jwtData.profileId;
  return next(); // MUST be called for the scenario to continue
}

function extractPurchaseIdOrUrl(requestParams, response, context, ee, next) {
  const body = JSON.parse(response.body);
  if(body._embedded && body._embedded.length > 0){
    context.vars.purchaseId = body._embedded[0].attributes.subscriptionInfo[0].purchaseId
  }else {
    context.vars.purchaseHref = [body._links.variant[0].href]
  }

  return next(); // MUST be called for the scenario to continue
}

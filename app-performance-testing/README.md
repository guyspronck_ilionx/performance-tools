# How to use this repository

First run `npm install` to install the required dependencies.
Then create a users.stg.csv and a users.local.csv (copy users.sample.csv and fill in with real user accounts).

Then you can run `npm run test:local` and `npm run test:stg` following the test you want to run.
Gor example: `npm run test:stg test/main.yml` and the shortcut is now `npm run stg:main`



```bash
http://localhost:8100/rest/auth/login

http://localhost:8100/rest/v/1/client/app/profile/232148542/view/profileview/fragment/subscriptionsfragment

http://localhost:8100/rest/v/1/client/app/profile/232148542/subscription/688400049/view/subscriptionview/fragment/subscriptionfragment

http://localhost:8100/rest/v/1/client/app/profile/232148542/subscription/688400049/view/subscriptionview/fragment/bucketfragment

http://localhost:8100/rest/v/1/client/app/profile/232148542/subscription/688400049/view/subscriptionview/fragment/subscriptionsettingsfragment

http://localhost:8100/rest/v/1/client/app/profile/232148542/subscription/688400049/view/profileview/fragment/profilefragment

http://localhost:8100/rest/v/1/client/app/profile/232148542/subscription/688400049/view/profileview/fragment/profilesettingsfragment
```

Query for active, provisioned users with password 'Passw0rd'

```sql
WITH SP_USERS AS (
SELECT COUNT(1), ID, PASSWORD FROM DPS_USER DU
JOIN OINB_USER_PURCHASES UP ON UP.USER_ID = DU.ID
GROUP BY DU.ID, DU.PASSWORD
HAVING COUNT(1) = 1
)
SELECT SPU.ID, PS.PUK, OU.LOGIN, SPU.PASSWORD
FROM SP_USERS SPU
JOIN OINB_USER OU ON SPU.ID = OU.USER_ID
JOIN OINB_USER_PURCHASES UP ON UP.USER_ID = SPU.ID
JOIN OINB_PURCHASE OP ON OP.PURCHASE_ID = UP.PURCHASE_ID
JOIN OINB_PLAN_PURCHASE OPP ON OPP.PURCHASE_ID = UP.PURCHASE_ID
JOIN OINB_PLAN_PURCHASE_VERSION PPV ON UP.PURCHASE_ID = PPV.PURCHASE_ID
JOIN OINB_SUBSCRIPTION_PURCHASE OSP ON OSP.PURCHASE_ID = UP.PURCHASE_ID
JOIN OINB_PURCHASE_SIM PS ON PS.SIM_ID = PPV.SIM_ID
WHERE PPV.IS_ACTIVE_VERSION = 1 AND PS.PUK IS NOT NULL
AND OP.DELETED = 0
AND OU.BRAND_ID = 'brand_lvr'
AND OPP.DEACTIVATION_DATE IS NULL
AND SPU.PASSWORD = 'd41e98d1eafa6d6011d3a70f1a5b92f0'
AND OSP.DUNNING_STATUS = 'NORMAL'
AND INSTR(OU.LOGIN,'_') = 0;
```
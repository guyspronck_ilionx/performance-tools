var jwtDecode = require('jwt-decode');
const fs = require('fs');

module.exports = {
  extractProfileId: extractProfileId,
  extractFragmentUrls: extractFragmentUrls,
  extractVariantUrls: extractVariantUrls,
  extractSubscriptionId: extractSubscriptionId,
  findToSingleMsisdnView: findToSingleMsisdnView,
  beginExtraRequest: beginExtraRequest,
  endExtraRequest: endExtraRequest
}

function extractProfileId(requestParams, response, context, ee, next) {
  var jwtToken = context.vars.jwtToken;
  var jwtData = jwtDecode(jwtToken);
  context.vars.profileId = jwtData.profileId;
  return next(); // MUST be called for the scenario to continue
}

function extractFragmentUrls(requestParams, response, context, ee, next) {
  context.vars.fragmentUrls = [];

  try{
    const body = JSON.parse(response.body);
    if(body._links && body._links.fragment){
      for (let i = 0; i < body._links.fragment.length; i++) {
        context.vars.fragmentUrls.push(body._links.fragment[i].href);
      }
    }
  }catch(err){
    console.log(err);
    console.log(response.body);
  }
  return next();
}


function extractSubscriptionId(requestParams, response, context, ee, next) {
  if(context.vars.subscriptionId === undefined){
    return next();
  }
  context.vars.subscriptionIdForStatement = context.vars.subscriptionId;
  return next();
}

// Variant Urls
function extractVariantUrls(requestParams, response, context, ee, next) {
  if(context.vars.variantUrls === undefined){
    context.vars.variantUrls = [];
  }
  try{
    const body = JSON.parse(response.body);
    context.vars.variantUrls = extractVariantUrlsHandleBody(body, context.vars.variantUrls);
  }catch(err){
    console.log(err);
    console.log(response.body);
  }
  return next();
}

function extractVariantUrlsHandleBody(body, variantUrls) {
  if(body._links && body._links.variant){
    for (let i = 0; i < body._links.variant.length; i++) {
      variantUrls.push(body._links.variant[i].href);
    }
  }
  if(body._embedded){
    for (let i = 0; i < body._embedded.length; i++) {
      const element = body._embedded[i];
      variantUrls = extractVariantUrlsHandleBody(element, variantUrls);
    }
  }
  return variantUrls;
}

// toSingleMdisdnView
function findToSingleMsisdnViewHandleBody(body) {
  // Check if current element has url
  if(body.fragmentId === "msisdninfo" && body.variantID === "activemsisdn"){
    for (let i = 0; i < body._links.event.length; i++) {
      const element = body._links.event[i];
      if(element.name == "toSingleMsisdnView"){
        href = element.href;
        return href;
      }
    }
  }
  // check if body contains url
  if(body._embedded){
    for (let i = 0; i < body._embedded.length; i++) {
      const element = body._embedded[i];
      var href = findToSingleMsisdnViewHandleBody(element);
      if(href){
        return href;
      }
    }
  }
  // Found nothing
  return;
}

function findToSingleMsisdnView(requestParams, response, context, ee, next) {
  const body = JSON.parse(response.body);
  var href = findToSingleMsisdnViewHandleBody(body);
  if(href){
    context.vars.toSingleMsisdnView = href;
  }else{
    console.log("href undefined ", context.vars.profileId);
    console.log(body);
  }
  return next();
}

var extraRequestPerformance = [];
fs.truncate('extraRequestData.csv', 0, function(){console.log('Cleaned extraRequestData.csv')});
var stream = fs.createWriteStream("extraRequestData.csv", {flags:'a'});
stream.write("url;duration\r\n");

function beginExtraRequest(requestParams, context, ee, next) {
  // start
  var uuid = context.vars.$uuid + "-" + context.vars.$loopElement;
  var extraRequest = {
    uuid,
    url: context.vars.$loopElement,
    beginTime: Date.now()
  }
  extraRequestPerformance[uuid] = extraRequest;
  return next();
}

function endExtraRequest(requestParams, response, context, ee, next) {
  var uuid = context.vars.$uuid + "-" + context.vars.$loopElement;

  extraRequest = extraRequestPerformance[uuid];
  extraRequest.endTime = Date.now();
  extraRequest.duration = extraRequest.endTime - extraRequest.beginTime;
  var csvLine = `${extraRequest.url};${extraRequest.duration}`;
  stream.write(csvLine + "\r\n");
  return next();
}
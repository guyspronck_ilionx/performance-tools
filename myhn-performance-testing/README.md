# How to use this repository

First run `npm install` to install the required dependencies.
Then create a users.stg.csv and a users.local.csv (copy users.sample.csv and fill in with real user accounts).

Then you can run `npm run test:local` and `npm run test:stg` following the test you want to run, for example: `npm run test:stg test/main.yml `